package by.bigsoft.news.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name = "topic")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "topic_id")
    private long topicId;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(name = "news_topic", joinColumns = { @JoinColumn(name = "topic_id", referencedColumnName = "topic_id") },
               inverseJoinColumns = { @JoinColumn(name = "news_id", referencedColumnName = "news_id") })
    private Set<News> news = new HashSet<News>();


    public Topic() {
    }

    public Topic(String name) {
        this.name = name;
    }


    public Topic(String name, Set<News> news) {
        this.name = name;
        this.news = news;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public long getTopicId() {
        return topicId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public Set<News> getNews() {
        return news;
    }

    @Override
    public int hashCode() {
        // TODO Implement this method
        return new Long(topicId).hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO Implement this method
        if (object == null)
            return false;

        if (object == this)
            return true;
        if (object.getClass() == this.getClass()) {
            Topic topic = (Topic) object;
            if (topic.name == this.name)
                return true;
        }
        return false;
    }
}
