package by.bigsoft.news.model;

import java.awt.Point;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "region")
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "region_id")
    private long regionId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "region")
    private Set<News> news = new HashSet<News>();


    public Region() {
    }

    public Region(String name) {
        this.name = name;
    }

    public Region(String name, Set<News> news) {

        this.name = name;
        this.news = news;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public Set<News> getNews() {
        return news;
    }


    @Override
    public int hashCode() {
        // TODO Implement this method
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO Implement this method
        if (object == null)
            return false;

        if (object == this)
            return true;
        if (object.getClass() == this.getClass()) {
            Region region = (Region) object;
            if (region.name == this.name)
                return true;
        }
        return false;
    }


}
