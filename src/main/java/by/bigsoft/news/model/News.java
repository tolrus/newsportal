package by.bigsoft.news.model;

import java.io.Serializable;

import java.util.Collection;
import java.util.Date;

import java.util.HashSet;
import java.util.Set;


import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;


import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import javax.persistence.Temporal;

import javax.persistence.TemporalType;


import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "news")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "news_id")
    private long newsId;

    @Column(name = "header")
    private String header;

    @Column(name = "body")
    @Type(type = "text")
    private String body;

    @Column(name = "title")
    @Type(type = "text")
    private String title;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Date date;

    @ManyToOne()
    @Cascade(value = CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "region_id", referencedColumnName = "region_id")

    private Region region;

    @ManyToMany(fetch = FetchType.EAGER)
    @Cascade(value = CascadeType.SAVE_UPDATE)    
    @JoinTable(name = "news_topic", joinColumns = { @JoinColumn(name = "news_id", referencedColumnName = "news_id") },
               inverseJoinColumns = { @JoinColumn(name = "topic_id", referencedColumnName = "topic_id") })
    private Set<Topic> topics = new HashSet<Topic>();


    public News() {
    }

    public News(String header, String body, String title, Region region, Date date, Set<Topic> topics) {

        this.header = header;
        this.body = body;
        this.title = title;
        this.region = region;
        this.date = date;
        this.topics = topics;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    public long getNewsId() {
        return newsId;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Date getDate() {
        return date;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setRegion(String region) {
        Region dopRegion = new Region(region);
        this.region = dopRegion;
    }

    @MapsId
    public Region getRegion() {
        return region;
    }


    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    public Set<Topic> getTopics() {
        return topics;
    }

}
