package by.bigsoft.news.dao;

import by.bigsoft.news.model.News;

import by.bigsoft.news.model.Region;

import by.bigsoft.news.model.Topic;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import java.util.Set;

import javax.annotation.Resource;

import javax.persistence.Id;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class NewsDaoImpl implements INewsDao {

    private SessionFactory sessionFactory;

    @Resource(name = "sessionFactory")
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public News save(News news) {
        // TODO Implement this method

        Session session = sessionFactory.getCurrentSession();

        Region existingRegion = news.getRegion();
        Region addRegion = this.findRegionByName(existingRegion.getName());
        Set<Topic> existingTopics = news.getTopics();
        Set<Topic> addTopics = new HashSet();
        for (Topic topic : existingTopics) {
            System.out.println(topic.getName());
            Topic newTopic = this.findTopicByName(topic.getName());
            addTopics.add(newTopic);
        }

        news.setRegion(addRegion);

        news.setTopics(addTopics);
        session.saveOrUpdate(news);
        return news;
    }

    @Override
    public News update(News news) {

        Session session = sessionFactory.getCurrentSession();
        News existingNews = (News) session.get(News.class, news.getNewsId());
        Region existingRegion = news.getRegion();
        Region addRegion = this.findRegionByName(existingRegion.getName());
        Set<Topic> existingTopics = news.getTopics();
        Set<Topic> addTopics = new HashSet();
        for (Topic topic : existingTopics) {
            System.out.println(topic.getName());
            Topic newTopic = this.findTopicByName(topic.getName());
            addTopics.add(newTopic);
        }
        existingNews.setBody(news.getBody());
        existingNews.setDate(news.getDate());
        existingNews.setHeader(news.getHeader());
        existingNews.setRegion(addRegion);
        existingNews.setTitle(news.getTitle());
        existingNews.setTopics(addTopics);
        session.saveOrUpdate(existingNews);
        return existingNews;

    }

    @Override
    public void delete(News news) {
        // TODO Implement this method
        sessionFactory.getCurrentSession().delete(news);

    }

    @Override
    public List<News> findAll() {
        // TODO Implement this method
        return sessionFactory.getCurrentSession().createQuery("select n from News n order by n.date desc").list();
    }

    @Override
    public News findById(long id) {
        // TODO Implement this method
        return (News) sessionFactory.getCurrentSession().createQuery("select n from News n where n.id=:id").setParameter("id",
                                                                                                                         id).uniqueResult();
    }

    @Override
    public List<News> findByRegion(long regionId) {
        // TODO Implement this method
        return sessionFactory.getCurrentSession().createQuery("select n from News n where n.region.regionId=:regionId").setParameter("regionId",
                                                                                                                                     regionId).list();
    }

    @Override
    public List<News> findByTopic(long topicId) {
        // TODO Implement this method
        return sessionFactory.getCurrentSession().createQuery("select n from News n left join n.topics topic where topic.topicId=:topicId").setParameter("topicId",
                                                                                                                                                         topicId).list();
    }

    @Override
    public List<Topic> findAllTopic() {
        // TODO Implement this method
        return sessionFactory.getCurrentSession().createQuery("select t from Topic t").list();
    }

    @Override
    public List<Region> findAllRegion() {
        // TODO Implement this method
        return sessionFactory.getCurrentSession().createQuery("select r from Region r").list();
    }

    @Override
    public Region findRegionByName(String name) {
        // TODO Implement this method
        return (Region) sessionFactory.getCurrentSession().createQuery("select r from Region r  where r.name=:name").setParameter("name",
                                                                                                                                  name).uniqueResult();

    }

    @Override
    public Topic findTopicByName(String name) {
        // TODO Implement this method
        return (Topic) sessionFactory.getCurrentSession().createQuery("select t from Topic t  where t.name=:name").setParameter("name",
                                                                                                                                name).uniqueResult();

    }
}
