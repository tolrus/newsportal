package by.bigsoft.news.dao;

import by.bigsoft.news.model.News;

import by.bigsoft.news.model.Region;

import by.bigsoft.news.model.Topic;

import java.util.List;

public interface INewsDao {

    public News save(News news);

    public News update(News news);

    public void delete(News news);

    public List<News> findAll();

    public News findById(long newsId);

    public List<News> findByRegion(long regionId);

    public List<News> findByTopic(long topicId);

    public List<Topic> findAllTopic();

    public Topic findTopicByName(String name);

    public List<Region> findAllRegion();

    public Region findRegionByName(String name);
}
