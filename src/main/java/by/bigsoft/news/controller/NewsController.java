package by.bigsoft.news.controller;

import by.bigsoft.news.dao.INewsDao;

import by.bigsoft.news.model.News;

import by.bigsoft.news.model.Region;

import by.bigsoft.news.model.Topic;
import by.bigsoft.news.util.Message;


import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequestMapping("/news")
@Controller
public class NewsController {

    @Autowired
    public INewsDao newsDao;
    @Autowired
    MessageSource messageSource;

//    @RequestMapping(method = RequestMethod.GET)
//    public String login() {
//
//        return "index";
//
//    }

    @RequestMapping( method = RequestMethod.GET)
    public String list(Model uiModel) {

        List<News> news = newsDao.findAll();
        uiModel.addAttribute("news", news);
        return "list";

    }

    @RequestMapping(value = "getbody", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    public @ResponseBody
    String getBody(@RequestParam("id") Integer id) {

        News news = newsDao.findById(id);

        return news.getBody();
    }

    @RequestMapping(value = "region", method = RequestMethod.GET)
    public String findByRegion(@RequestParam("regionId") Long regionId, Model uiModel) {

        List<News> news = newsDao.findByRegion(regionId);
        uiModel.addAttribute("news", news);
        return "list";
    }

    @RequestMapping(value = "topic", method = RequestMethod.GET)
    public String findByTopic(@RequestParam("topicId") Long topicId, Model uiModel) {

        List<News> news = newsDao.findByTopic(topicId);
        uiModel.addAttribute("news", news);
        return "list";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id") Long id, Model uiModel) {

        News news = newsDao.findById(id);
        uiModel.addAttribute("news", news);
        return "show";
    }

    @RequestMapping(value = "{id}", params = "form", method = RequestMethod.POST)
    public String update(News news, BindingResult bindingResult, Model uiModel, RedirectAttributes redirectAttributes,
                         Locale locale) {

        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("message", new Message("error", messageSource.getMessage("contact_save_fail", new Object[] {
                                                                                          }, locale)));
            uiModel.addAttribute("news", news);
            List error = new ArrayList();
            error = bindingResult.getAllErrors();
            for (int i = 0; i < error.size(); i++) {
                System.out.println(error.get(i));
            }
            return "edit";

        }
        uiModel.asMap().clear();
        redirectAttributes.addFlashAttribute("message",
                                             new Message("success", messageSource.getMessage("contact_save_success", new Object[] {
                                                                                             }, locale)));
        newsDao.update(news);
        return "redirect:/news/" + news.getNewsId();
    }

    @RequestMapping(value = "{id}", params = "form", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        News news = newsDao.findById(id);
        List<Topic> topicList = newsDao.findAllTopic();
        List<Region> topRegion = newsDao.findAllRegion();
        uiModel.addAttribute("news", news);
        uiModel.addAttribute("topicList", topicList);
        uiModel.addAttribute("topRegion", topRegion);
        return "edit";
    }

    @RequestMapping(value = "/", params = "form", method = RequestMethod.POST)
    public String create(News news, BindingResult bindingResult, Model uiModel, RedirectAttributes redirectAttributes,
                         Locale locale) {

        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("message", new Message("error", messageSource.getMessage("contact_save_fail", new Object[] {
                                                                                          }, locale)));
            uiModel.addAttribute("news", news);
            List error = new ArrayList();
            error = bindingResult.getAllErrors();
            for (int i = 0; i < error.size(); i++) {
                System.out.println(error.get(i));
            }
            return "edit";

        }
        uiModel.asMap().clear();
        redirectAttributes.addFlashAttribute("message",
                                             new Message("success", messageSource.getMessage("contact_save_success", new Object[] {
                                                                                             }, locale)));
        newsDao.save(news);
        return "redirect:/news/" + news.getNewsId();
    }

    @RequestMapping(value = "/", params = "form", method = RequestMethod.GET)
    public String createForm(Model uiModel) {
        News news = new News();
        List<Topic> topicList = newsDao.findAllTopic();
        List<Region> topRegion = newsDao.findAllRegion();
        uiModel.addAttribute("news", news);
        uiModel.addAttribute("topicList", topicList);
        uiModel.addAttribute("topRegion", topRegion);
        return "create";
    }

    @RequestMapping(value = "{id}", params = "delete", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id, Model uiModel) {
        News news = newsDao.findById(id);
        newsDao.delete(news);
        List<News> UpdtNews = newsDao.findAll();
        uiModel.addAttribute("news", UpdtNews);
        return "list";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

}

