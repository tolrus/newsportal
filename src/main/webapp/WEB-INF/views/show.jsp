<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>News information</title>
        <spring:url value="/resources/scripts/jquery-2.1.0.js" var="jquery_url"/>
        <spring:message code="label_news_info" var="labelNewsInfo"/>        
        <spring:url value="/news" var="editNewsUrl"/>
        <spring:url value="/news/${news.newsId}" var="homeUrl"/>
    </head>
    <body>
        <h1>
            ${labelNewsInfo}
        </h1>
         
        <c:if test="${not empty news}">
            <div id="newstInfo">
                <c:if test="${not empty message}">
                    <div id="message" class="${message.type}">
                        ${message.message}
                    </div>
                </c:if>
                <div>
                    ${news.header}
                </div>
                <br/>
                <div>
                    ${news.region.name}
                </div>
                <div>
                    ${news.date}
                </div>
                <br/>
                <div>
                    <c:forEach items="${news.topics}" var="topics">
                        <div>
                            ${topics.name}
                        </div>
                    </c:forEach>
                    <div></div>
                    <br/>
                </div>
                <div>
                    <div>
                        ${news.title}
                    </div>
                    <div>
                        ${news.body}
                    </div>
                    <br/>
                </div>
                <br/>
            </div>
        </c:if>
        <div>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <a href="${editNewsUrl}/${news.newsId}?form">Обновить новость</a>
            </sec:authorize>
        </div>
         <div>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <a href="${editNewsUrl}/${news.newsId}?delete">Удалить новость</a>
            </sec:authorize>
        </div>

    </body>
</html>