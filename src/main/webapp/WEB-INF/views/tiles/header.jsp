<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Header</title>
        <spring:url value="/resources/styles/images/logo.png" var="logo_url"/>
    </head>
    <body>
        <img id="logo" src="${logo_url}" height="98" width="404"/>
    </body>
</html>