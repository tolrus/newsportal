<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8"/>
        <spring:message code="label_login_error" var="loginError"/>
    </head>
    <body>
        <c:if test="${not empty param.error}">
            
            <div style="color:Red;">
                ${loginError}
            </div>
        </c:if>
         
        <form method="POST" action="<c:url value="/j_spring_security_check"/>">
            <div>
                <div>Login</div>
                <input type="text" name="j_username"/>
            </div>
            <div>
                <div>Password</div>
                <input type="password" name="j_password"/>
            </div>
            <div>
                <input type="submit" value="Login"/>
                 
                <input type="reset" value="Reset"/>
            </div>
        </form>
    </body>
</html>