<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <spring:message code="label_news_new" var="labelNewsNew"/>
        <spring:message code="label_news_update" var="labelNewsUpdate"/>
        <spring:message code="label_news_header" var="labelNewsHeader"/>
        <spring:message code="label_news_title" var="labelNewsTitle"/>
        <spring:message code="label_news_body" var="labelNewsBody"/>
        <spring:message code="label_news_region" var="labelNewsRegion"/>
        <spring:message code="label_news_topics" var="labelNewsTopics"/>
        <spring:message code="label_news_date" var="labelNewsDate"/>
        <spring:eval expression="news.newsId == null ? labelNewsNew:labelNewsUpdate" var="formTitle"/>
    </head>
    <body>
        <h1>
            ${formTitle}
        </h1>
        <div id="newsUpdate">
            <form:form  modelAttribute="news" method="POST">
                <c:if test="${not empty message}">
                    <div id="message" class="${message.type}">
                        ${message.message}
                    </div>
                </c:if>
                <div>
                    <form:label path="header">
                        ${labelNewsHeader}
                    </form:label>
                </div>
                <div>
                    <form:textarea path="header" cols="60" rows="2" id="newsHeader"/>
                </div>
                <div>
                    <form:label path="title">
                        ${labelNewsTitle}
                    </form:label>
                </div>
                <div>
                    <form:textarea path="title" cols="60" rows="3" id="newsTitle"/>
                </div>
                <div>
                    <form:label path="body">
                        ${labelNewsBody}
                    </form:label>
                </div>
                <div>
                    <form:textarea path="body" cols="60" rows="10" id="newsTitle"/>
                </div>
                <div>
                    <form:label path="region">
                        ${labelNewsRegion}
                    </form:label>
                </div>
                <div>
                    <form:select path="region.name">
                        <form:options items="${topRegion}"  itemValue="name" itemLabel="name" />
                        
                    </form:select>
                    
                </div>
                <div>
                    <form:label path="topics">
                        ${labelNewsTopics}
                    </form:label>
                </div>
                <div>
                    <form:checkboxes items="${topicList}" path="topics" itemValue="name" itemLabel="name"/>
                </div>
               
                <div>
                    <form:label path="date">
                        ${labelNewsDate}
                    </form:label>
                </div>
                <div>
                    <form:input path="date"/>
                </div>
                <form:button type="submit">Save</form:button>
                <form:button type="reset">Reset</form:button>
                <form:hidden path="newsId"/>
                
            </form:form>
        </div>
    </body>
</html>