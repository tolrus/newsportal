<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>News</title>
        <spring:url value="/resources/scripts/jquery-2.1.0.js" var="jquery_url"/>
        <spring:url value="/news/getbody" var="ajax_url"/>
        <spring:url value="/news/region" var="region_url"/>
        <spring:url value="/news/topic" var="topic_url"/>
        <spring:url value="/news" var="homeUrl"/>
        <spring:url value="/news" var="showNewsUrl"/>
        <spring:message code="label_news_list" var="labelNewsList"/>
        <spring:message code="label_en_US" var="labelUs"/>
        <spring:message code="label_ru_RU" var="labelRu"/>
        <spring:theme code="styleSheet" var="app_css"/>
        <spring:url value="/${app_css}" var="app_css_url"/>
        <link rel="stylesheet" type="text/css" media="screen" href="${app_css_url}"/>
        <script src="${jquery_url}" type="text/javascript"></script>
        <script type="text/javascript">
          function doAjax(reqUrl) {
              $.get('${ajax_url}', 
              {
                  id : reqUrl
              },
              function (response) {
                  $("#body" + reqUrl).html(response);
              });

          };
        </script>
    </head>
    <body>
        <h1>
            ${labelNewsList}
        </h1>
         
        <c:if test="${not empty news}">
            <c:forEach items="${news}" var="news">
                <div>
                    <div>
                        <a href="${showNewsUrl}/${news.newsId}">
                            ${news.header}</a>
                    </div>
                    <br/>
                    <div>
                        <a href="${region_url}?regionId=${news.region.regionId}">
                            ${news.region.name}</a>
                    </div>
                    <div>
                        ${news.date}
                    </div>
                    <br/>
                    <div>
                        <c:forEach items="${news.topics}" var="topics">
                            <div>
                                <a href="${topic_url}?topicId=${topics.topicId}">
                                    ${topics.name}</a>
                            </div>
                        </c:forEach>
                        <div></div>
                        <br/>
                    </div>
                    <div>
                        <div onclick="doAjax(${news.newsId})">
                            ${news.title}
                        </div>
                        <div id="body${news.newsId}"></div>
                        <br/>
                    </div>
                    <br/>
                </div>
            </c:forEach>
        </c:if>
        <div>
            <a href="${homeUrl}?lang=en_US">
                ${labelUs}</a>
             
            <a href="${homeUrl}?lang=ru_RU">
                ${labelRu}</a>
        </div>
        <div>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <a href="${homeUrl}/?form">Добавить новость</a>
            </sec:authorize>
        </div>
    </body>
</html>