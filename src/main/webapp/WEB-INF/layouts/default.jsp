<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=8"/>
        <spring:theme code="styleSheet" var="app_css"/>
        <spring:url value="/${app_css}" var="app_css_url"/>
        <link rel="stylesheet" type="text/css" media="screen" href="${app_css_url}"/>
        <!-- jQuery and jQuery UI -->
        <spring:url value="/resources/scripts/jquery-2.1.0.js" var="jquery_url"/>
        <spring:url value="/resources/scripts/jquery-ui-1.10.4.min.js" var="jquery_ui_url"/>
        <spring:url value="/resources/styles/custom-theme/jquery-ui-1.10.4.css" var="jquery_ui_theme_css"/>
        <link rel="stylesheet" type="text/css" media="screen" href="${jquery_ui_theme_css}"/>
        <script src="${jquery_url}" type="text/javascript"></script>
        <script src="${jquery_ui_url}" type="text/javascript"></script>
        <core:set var="userLocale">
            <core:set var="plocale">
                ${pageContext.response.locale}
            </core:set>
            <core:out value="${fn:replace(plocale, '_', '-')}" default="en"/>
        </core:set>
        <title>Portal</title>
    </head>
    <body>
        <div id="headerWrapper">
            <tiles:insertAttribute name="header" ignore="true"/>
             
            <tiles:insertAttribute name="menu" ignore="true"/>
        </div>
        <div id="main">
            <tiles:insertAttribute name="body"/>
             
            <tiles:insertAttribute name="footer" ignore="true"/>
        </div>
    </body>
</html>